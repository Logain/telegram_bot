import os
import sys
import logging

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

# Video = message.reply_to_message.video
file_path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/tmp/{}_{}.webm'


def reply_with_mp4(path, bot, update):

    bot.send_document(update.message.chat.id, open(path, 'rb'))


def convert_wbm_file_to_mp4_file(webm_file):
    mp4_file = webm_file.replace('.webm', '.mp4')
    cmd_string = 'ffmpeg -i "' + webm_file + '" "' + mp4_file + '"'
    os.system(cmd_string)
    return mp4_file


def webm_handler(bot, update):
    logger.warning('Sosat "%s"', update)
    download(bot, update)

def cleanup(path):
    cmd_string = 'rm -rf '+path
    os.system(cmd_string)
    cmd_string = cmd_string.replace('.mp4', '.webm')
    os.system(cmd_string)

def download(bot, update):
    file_id = update.message.document
    new_file = bot.get_file(file_id)
    path = str(file_id.file_id)+".webm"
    new_file.download(path)
    path = convert_wbm_file_to_mp4_file(path)
    reply_with_mp4(path, bot, update)
    print(path)
    cleanup(path)
